# fingerprintRGBtoBW

## Structure and installation

The main FIJI/ImageJ script is `fingerprintRGBtoBW.ijm` in the root directory.

Needs a FIJI distribution (https://imagej.net/software/fiji/downloads) in a subdirectory (any name, e.g. `/Fiji`).
Required update sites (add to a default distribution via `Help->Update...`, `Manage update sites`):
- ImageJ (`https://update.imagej.net/`)
- Fiji (`https://update.fiji.sc/`)
- Java-8 (`https://sites.imagej.net/Java-8/`)
- IJ-Plugins (`https://sites.imagej.net/IJ-Plugins/`)
- IJPB-plugins (`https://sites.imagej.net/IJPB-plugins/`)
- UCB Vision Sciences (`https://sites.imagej.net/Llamero/`)

If the Gabor filtering step should be used, needs Python and the content of https://github.com/172454/Fingerprint-Enhancement-Python in the `/Fingerprint-Enhancement-Python-master` subdirectory.

(If different locations should be used, some hard-coded paths will have to be edited inside `fingerprintRGBtoBW.ijm`, and possibly changed from relative to absolute paths.)

## Running the script

Run using the FIJI distribution inside a subdirectory.

Because of the way FIJI browses for files, the script has to be launched using `Plugins->Macros->Edit...` in the main FIJI window,selecting `fingerprintRGBtoBW.ijm`, and then clicking `Run` or `[Ctrl+R]` in the editor window.

(Simple `Plugins->Macros->Run...` WILL NOT WORK, and will instead produce a `Statement cannot begin with '#'` error message.)

## Input

RGB image, side-illuminated finger on a smooth black background, optionally next to a coin  for scale (preferably 1 CZK; otherwise, its diameter in mm will have to be updated). No other significant edges in the image (e.g. an edge of a table, other fingers).

Do not open the image in advance, the script will ask you to browse for it.

## Parameters

Can be specified via GUI (after the first computation that runs with the default values), or modified inside `fingerprintRGBtoBW.ijm`, in the initial section marked as `SETUP BLOCK`.

## Authors

`fingerprintRGBtoBW.ijm`: Karel Štěpka, CBIA FI, Masaryk University (172454 at mail.muni.cz)

`Fingerprint-Enhancement-Python`: Forked from https://github.com/ylevalle/Fingerprint-Enhancement-Python (Yamila Levalle) and updated to work on Python 3.


